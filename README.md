# Sample-CSVs
The CSVs contain details of fake people. The following example shows the type of records:
| id     |  name               |  email                   |  city          |  phone          |  created_at                                                     |  updated_at                                                     |
|--------|---------------------|--------------------------|----------------|-----------------|-----------------------------------------------------------------|-----------------------------------------------------------------|
| 855656 |  Gayle Rau          |  Vicky.Green@hotmail.com |  Everett       |  (471) 370-0008 |  Sat Nov 06 2021 10:07:49 GMT+0000 (Coordinated Universal Time) |  Sat Nov 06 2021 10:07:49 GMT+0000 (Coordinated Universal Time) |
| 855657 |  Lula Brown         |  Donato_Little@gmail.com |  Jefferyfort   |  (292) 641-3568 |  Sat Nov 06 2021 10:07:49 GMT+0000 (Coordinated Universal Time) |  Sat Nov 06 2021 10:07:49 GMT+0000 (Coordinated Universal Time) |
| 855658 |  Miss Terrence Ward |  Shayne31@yahoo.com      |  MacGyvershire |  (449) 813-7933 |  Sat Nov 06 2021 10:07:49 GMT+0000 (Coordinated Universal Time) |  Sat Nov 06 2021 10:07:49 GMT+0000 (Coordinated Universal Time) |
